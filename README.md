# Ansible Role: Kubernetes

An Ansible Role that installs [Kubernetes](https://kubernetes.io) on Linux.

## Requirements

On the managed nodes, you need a way to communicate, which is normally ssh. By default this uses sftp. If that’s not available, you can switch to scp in ansible.cfg. You also need Python 2 (version 2.6 or later) or Python 3 (version 3.5 or later).

Install ansible

On Fedora:
```sh
$ sudo dnf install ansible
```

On RHEL and CentOS:
```sh
$ sudo yum install ansible
```

Latest Releases via Apt (Debian):

```sh
 $ sudo apt update
 $ sudo apt install software-properties-common
 $ sudo apt-add-repository --yes --update ppa:ansible/ansible
 $ sudo apt install ansible
```

## Run
ansible-playbook -i inventory/configure_kubernates_cluster install_kubernates_cluster.yml -v