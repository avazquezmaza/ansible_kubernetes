---
- name: "Initialize Kubernetes master with kubeadm init."
  command: >
    kubeadm init
    --pod-network-cidr={{ kubernetes_pod_network_cidr }}
    --apiserver-advertise-address={{ kubernetes_apiserver_advertise_address | default(ansible_default_ipv4.address, true) }}
    --kubernetes-version {{ kubernetes_version_kubeadm }}
    --ignore-preflight-errors={{ kubernetes_ignore_preflight_errors }}
    {{ kubernetes_kubeadm_init_extra_opts }}
  register: kubeadmin_init
  failed_when: false
  when: not kubernetes_init_stat.stat.exists

- name: Print the init output to screen.
  debug:
    var: kubeadmin_init.stdout
    verbosity: 2
  when: not kubernetes_init_stat.stat.exists

- name: Ensure .kube directory exists.
  file:
    path: "{{ item.dir }}"
    state: directory
    owner: "{{ item.owner }}"
    group: "{{ item.group }}"
  with_items:
  - { dir: '/home/{{user_kube}}/.kube', owner: "{{ user_kube }}", group: "{{ group_kube }}" }
  - { dir:  '~/.kube', owner: root, group: root }

- name: Copy /etc/kubernetes/admin.conf to $HOME/.kube/config
  copy:
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
    owner: "{{ item.owner }}"
    group: "{{ item.group }}"
    remote_src: yes
  with_items:
  - { src: '/etc/kubernetes/admin.conf', dest: '/home/{{user_kube}}/.kube/config', owner: "{{ user_kube }}", group: "{{ group_kube }}" }
  - { src: '/etc/kubernetes/admin.conf', dest: '/root/.kube/config', owner: root, group: root }

- name: chown $HOME/.kube/config
  file:
    path: /home/{{user_kube}}/.kube/config
    owner: "{{ user_kube }}"
    group: "{{ group_kube }}"
    mode: 0644

- name: Copy "kube-flannel.yml" to remote
  copy:
    src: kube-flannel.yml
    dest: /tmp/kube-flannel.yml
    mode: '0744'

- name: Configure Flannel networking.
  command: "{{ item }}"
  with_items:
    - kubectl create -f /tmp/kube-flannel.yml
  register: flannel_result
  changed_when: "'created' in flannel_result.stdout"

# - name: Configure Flannel networking.
#   command: "{{ item }}"
#   with_items:
#     - kubectl apply -f {{ kubernetes_flannel_manifest_file_rbac }}
#     - kubectl apply -f {{ kubernetes_flannel_manifest_file }}
#   register: flannel_result
#   changed_when: "'created' in flannel_result.stdout"


# TODO: Check if taint exists with something like `kubectl describe nodes`
# instead of using kubernetes_init_stat.stat.exists check.
- name: Allow pods on master node (if configured).
  command: "kubectl taint nodes --all node-role.kubernetes.io/master-"
  when:
    - kubernetes_allow_pods_on_master
    - not kubernetes_init_stat.stat.exists

- name: Check if Kubernetes Dashboard UI service already exists.
  shell: kubectl get services --all-namespaces | grep -q kubernetes-dashboard
  changed_when: false
  failed_when: false
  register: kubernetes_dashboard_service
  when: kubernetes_enable_web_ui

- name: "Enable the Kubernetes Web Dashboard UI (if configured). {{kubernetes_dashboard_service.rc}}"
  command: "kubectl apply -f {{ kubernetes_web_ui_manifest_file }}"
  when: 
    - kubernetes_enable_web_ui
    - kubernetes_dashboard_service.rc > 0

- name: cp dashboard user yamls 
  copy: 
    src: "{{ item.src }}"
    dest: "{{ item.dest }}"
  with_items:
    - { src: 'files/dashboard-adminuser.yaml', dest: '/tmp/dashboard-adminuser.yaml'}
    - { src: 'files/dashboard-cluster-role-binding.yaml', dest: '/tmp/dashboard-cluster-role-binding.yaml'}

- name: create user dashboard
  command: "{{ item }}"
  with_items:
  - kubectl apply -f /tmp/dashboard-adminuser.yaml
  - kubectl apply -f /tmp/dashboard-cluster-role-binding.yaml